import 'cypress-file-upload'

describe('Test', () => {
    before(function () {
        cy.LogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
    })

    beforeEach(function () {
        Cypress.Cookies.preserveOnce('idsrv.session')
    })

    it('Динамические селекторы', () => {
        //cy.get('input.q-field__native.q-placeholder[aria-label="Поиск по тексту"][tabindex="0"][id^="f_"]') 

    })

    it('Дропдауны на странице списка отелей',()=>{
        /*cy.get(':nth-child(7) > .q-btn__wrapper').click()
        cy.get('[href="/crs/admin/hotels"]').click({ force: true })
        cy.url().should('include','/crs/admin/hotels')

        cy.get('.q-select__dropdown-icon').click()
        cy.get('.q-virtual-scroll__content > :nth-child(3)').click()

        //поиск по названию отеля внутри таблицы с отелями
        cy.get('.workarea > :nth-child(1) > .page > :nth-child(1)').contains('Кавказ').click()*/
    })
    
    it('Загрузка файлов',()=>{
        cy.get('[href="/sanatop/private/tasks"] > .q-btn__wrapper').click()
        cy.url().should('include','/sanatop/private/tasks')
        cy.get('tbody > :nth-child(1) > :nth-child(1)').click()
        cy.get(':nth-child(4) > :nth-child(1) > :nth-child(1) > .q-btn__wrapper').click()

        //загрузка файлов с помощью плагина для сайпресса
        cy.get('.q-uploader__input').attachFile('correctCSV.csv')


        //как это работает в селениуме
        //cy.get('.q-uploader__input').type('/Users/volodya315/CypressFacebook/cypress/fixtures/correctCSV.csv')
    })
})

