describe('back request', () => {
    before(function () {
        cy.LogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
    })

    beforeEach(function () {
        Cypress.Cookies.preserveOnce('idsrv.session')
    })

    it('Авторизуемся напрямую через бекенд', () => {
        cy.request({
            url: 'https://sts.sanatop-test.quirco.com/connect/token',
            method: 'POST',
            form: true,
            body: {
                grant_type: 'password',
                username: 'admin@sanatop.ru',
                password: 'siUTyhbDgzS6f8yr',
                client_secret: 'r9WPw4cEcBSXNEbx',
                client_id: 'backend-dev'
            }
        }).then((response) => {
            expect(response.status).to.eq(200)
            cy.wrap(response.body.access_token).as('token')
        })
    })
    it('Используем токен авторизации в запросе', function () {
        const token = this.token
        cy.log(token)

        cy.request({//создаем новый отель через апи
            url: 'https://sanatop-test.quirco.com​/api/crs/hotels',
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`//используем токен из предыдущего теста
            },
            form: false,
            body: {
                "code": "sh",
                "name": "silent hill",
                "description": "cute cozy places full of monsters",
                "checkInTime": "24:00",
                "checkOutTime": "03:00",
                "dayCheckInTime": "14:00",
                "dayCheckOutTime": "07:00",
                "pmsToken": "22222",
                "lastDeliveryTime": "2020-11-08T14:49:46.003Z",
                "notifyEmail": "Pyramid.Head@SilentHill.com",
                "serviceEmail": "Pyramid.Head@SilentHill.com",
                "bookingHorizonDays": 0
              }
        })
    })
    it('Ожидает по урлу и запросу на бекенд', () => {
        cy.server()//команда для работы с бекендом
        cy.route('GET', 'https://sanatop-test.quirco.com/sanatop/admin/sanatoriums').as('sanatoriums')//роут и алиас запроса
    })   

})