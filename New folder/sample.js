//Описание файла спецификации. "Способы оплаты"
describe('Payment methods', () => {
    //функция, которая выполняется в самом начале перед тестами. В нашем случае - авторизация. При возникновении ошибок тесты не запускаются
    before(function () {
        cy.LogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
    })

    //функция, которая выполняется перед каждым тестом. В нашем случае - сохранение куков, чтобы не разлогинило
    beforeEach(function () {
        Cypress.Cookies.preserveOnce('idsrv.session')
    })

    //тест. "Это переходит вкладку способов оплаты"
    it('Naviges to payment methods tab', () => {
        //Описываем, что нам нужно для теста: переменные/константы, запросы к апи и тд
        cy.server()
        cy.route('GET', '/api/orders/payment-methods?PageNo=1&Size=20').as('paymentsList')

        //шаги
        cy.get(':nth-child(6) > .q-btn__wrapper').click()
        cy.get('[href="/sanatop/admin/payment-types"]').click({ force: true })

        //ожидаемый результат
        cy.url().should('include', '/sanatop/admin/payment-types')
        cy.get('.text-h5').should('contain', 'Виды оплат')
        cy.wait('@paymentsList').then((response) => {
            expect(response.status).to.eq(200)
            expect(response.responseBody).property('count').to.eq(32)
        })
    })
}) 