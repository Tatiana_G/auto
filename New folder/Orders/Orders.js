describe('Orders', () => {
    beforeEach(() => {
        // before each test, we can automatically preserve the
        // 'session_id' and 'remember_token' cookies. this means they
        // will not be cleared before the NEXT test starts.
        //
        // the name of your cookies will likely be different
        // this is an example
        Cypress.Cookies.preserveOnce('sid', 'access_token')
      })
    it('Navigate to log in page', () => {
        cy.visit('https://sanatop-test.quirco.com/');
        cy.get('#Username').type('admin@sanatop.ru') //input login
        cy.get('#Password').type('siUTyhbDgzS6f8yr') //input password
        cy.get('.cc-btn').click() 
        cy.get('.col-sm-12 > .btn-primary').click() 
        cy.url().should('eq', 'https://sanatop-test.quirco.com/sanatop/private/orders') // => true
    })
    it('create new sanatorium', () =>{
        cy.get('[href="/sanatop/admin/sanatoriums"]').click();
        cy.get('.q-mr-md > .q-btn > .q-btn__wrapper > .q-btn__content > .block').click();
        cy.get('#f_d0b00da6-ced0-4389-b5f4-bc0cd0f9e6b3').type('Mriya')
        cy.get('.q-select--with-input > .q-field__inner > .q-field__control > .q-field__control-container').type ('Yalta')

    })
});
