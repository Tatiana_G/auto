describe('sample', () => {
    before(function () {
        cy.LogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
    })

    beforeEach(function () {
        Cypress.Cookies.preserveOnce('idsrv.session')
    })

    it('Ожидает по урлу и запросу на бекенд', () => {
        cy.server()//команда для работы с бекендом
        cy.route('GET', '/api/orders/tasks?PageNo=1&Size=100').as('tasks')//роут и алиас запроса

        cy.get('[href="/sanatop/private/tasks"] > .q-btn__wrapper').click()//переходим на страницу задач
        cy.url().should('include', '/sanatop/private/tasks')//ассерт по урлу

        cy.wait('@tasks').then((response) => {//ассерты по ответу от сервера
            expect(response.status).to.eq(200)//проверяем, что код ответа на запрос равен коду 200
            expect(response.responseBody.count).to.eq(1705)//проверяем, что у нас 1705 задач возвращается с бекенда
        }).then((request) => {//ассерты по запросу на сервер
            expect(request.requestBody).to.eq(null)//проверяем, что на бекенд в теле запроса ничего не отправляется
        })
    })

    it('Ожидает по элементам', () => {
        cy.get('.blocker > .q-linear-progress > .q-linear-progress__model').should('not.be.visible')//проверяем, что наш лоадер исчез
        cy.get('tbody > :nth-child(1) > :nth-child(1)').should('be.visible')//проверяем отображание элемента в таблице задач
    })

    it('Авторизуемся напрямую через бекенд', () => {
        cy.request({
            url: 'https://sts.sanatop-test.quirco.com/connect/token',
            method: 'POST',
            form: true,
            body: {
                grant_type: 'password',
                username: 'admin@sanatop.ru',
                password: 'siUTyhbDgzS6f8yr',
                client_secret: 'r9WPw4cEcBSXNEbx',
                client_id: 'backend-dev'
            }
        }).then((response) => {
            expect(response.status).to.eq(200)
            cy.wrap(response.body.access_token).as('token')
        })
    })

    it('Используем токен авторизации в запросе', function () {
        const token = this.token
        cy.log(token)

        cy.request({//создаем новый город через апи
            url: 'https://sanatop-test.quirco.com​/api/catalog/cities',
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`//используем токен из предыдущего теста
            },
            form: false,
            body: {
                name: "asddasd"
            }
        })
    })
})