describe('Creates payment method', () => {
    //an example
      })
it('Creates payment method', () => {
NavigateToMethods()

cy.route('POST', '/api/orders/payment-methods').as('payment')
cy.get(payments.createPaymentMethod).click({ force: true })
cy.url().should('contain', setupData.urlPaymentCreate)

cy.get(payments.methodTitle).type('Cypress test payment')
cy.get(payments.saveBtn).click()
Actions.AssertResponseCode('@payment', 201)
})

it('Edits payment method', () => {
cy.server()
cy.route('PUT', '/api/orders/payment-methods/*').as('edited')
cy.get(payments.methodTitle).type('{selectall}{backspace}New payment')
cy.get(payments.saveBtn).first().click()
Actions.AssertResponseCode('@edited', 200)
})

it('Deletes payment method', () => {
cy.server()
cy.route('DELETE', '/api/orders/payment-methods/*').as('deleted')
cy.get(payments.deletBtn).last().click()
cy.get(payments.yesDlg).click()
Actions.AssertResponseCode('@deleted', 204)
})