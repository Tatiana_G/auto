 describe('Create blocks', () => {

     before(function () {
         cy.testlogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
     })


     beforeEach(function () {
         Cypress.Cookies.preserveOnce('idsrv.session')
         cy.clearLocalStorage()
     })


     function RandomInt(min, max) {
         min = Math.ceil(min);
         max = Math.floor(max);
         return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
     }


     it('Navigate to sanatorium tab', () => {
         cy.server()
         cy.route('GET', '/api/sanatoriums?PageNo=1&Size=20').as('sanatoriums')

         cy.get(':nth-child(6) > .q-btn__wrapper > .q-btn__content > .material-icons').click()
         cy.get('[href="/sanatop/admin/sanatoriums"]').click({
             force: true
         })

         cy.url().should('include', '/sanatop/admin/sanatoriums')
         cy.get('.text-h5').should('contain', 'Санатории')
         cy.wait('@sanatoriums').then((response) => {
             expect(response.status).to.eq(200)
         })
     })


     it('Create Sanatorium', () => {
         cy.get('.q-mr-md > .q-btn > .q-btn__wrapper').click()
         cy.get('[aria-label="Название санатория"]').type('Silent Hill')
         cy.get('.col-sm-6.q-select--with-input > .q-field__inner > .q-field__control > .q-field__append > .q-select__dropdown-icon').click()
         cy.get('.q-item__label').contains('Омск').click()
         cy.get('[aria-label="Адрес сайта"]').type('https://en.wikipedia.org/wiki/Silent_Hill')
         cy.get('[aria-label="Максимальная цена"]').type('{Backspace}25000')
         cy.get('[aria-label="Код санатория"]').type(RandomInt(100, 500))
         cy.get('.q-select--without-input > .q-field__inner > .q-field__control > .q-field__control-container > .q-field__native').click()
         cy.get('.q-virtual-scroll__content > :nth-child(2)').click()
         cy.get('.text-right > .q-btn > .q-btn__wrapper').click() //click save buttom

         cy.get('.noty_body').should('contain', 'Санаторий успешно сохранен.').click({
             force: true
         }) //saved sanatorium check

     })


     it('Created sanatorium check', () => {
         cy.server()
         cy.route('GET', '/api/sanatoriums?PageNo=1&Size=20').as('sanatoriums')
         cy.clearLocalStorage()

         cy.get('.router-link-active').click()
         cy.url().should('include', '/sanatop/admin/sanatoriums')
         cy.get(':nth-child(3) > :nth-child(5) > .q-btn__wrapper > .q-btn__content > .material-icons').click()

         cy.get('.blocker > .q-linear-progress > .q-linear-progress__model').should('not.be.visible')
         cy.get('.workarea > :nth-child(1) > .page > :nth-child(1)').contains('Silent Hill')
         cy.wait('@sanatoriums').then((response) => {
             expect(response.status).to.eq(200)
         })
     })


     it('Navigate to Hotels tab', () => {
         cy.server()
         cy.route('GET', '/api/crs/hotels?PageNo=1&Size=20').as('hotels')
         cy.clearLocalStorage()
         cy.get(':nth-child(6) > .q-btn__wrapper > .q-btn__content > .material-icons').click()
         cy.get('[href="/crs/admin/hotels"]').click({
             force: true
         })

         cy.url().should('include', '/crs/admin/hotels')
         cy.get('.text-h5').should('contain', 'Отели')
         cy.wait('@hotels').then((response) => {
             expect(response.status).to.eq(200)
         })
     })


     it('Create blocks', () => {
             cy.get(':nth-child(3) > :nth-child(5) > .q-btn__wrapper > .q-btn__content > .material-icons').click()
             cy.get('.workarea > :nth-child(1) > .page > :nth-child(1)').contains('Silent Hill').click()
             cy.get('.blocker > .q-linear-progress > .q-linear-progress__model').should('not.be.visible')
             cy.get('.q-tab__content > .q-tab__label').contains('Блоки').click()

             cy.get('.text-right > .q-btn > .q-btn__wrapper').click()
             cy.get('.text-h6').should('contain', 'Новый блок объекта')
             cy.get('input.q-field__native.q-placeholder[aria-label="Код"][tabindex="0"][id^="f_"]').type('17')
             cy.get('input.q-field__native.q-placeholder[aria-label="Название"][tabindex="0"][id^="f_"]').type('rusted')
             cy.get('.q-ml-sm > .q-btn__wrapper > .q-btn__content > .block').click()

             cy.get('.noty_body').should('contain', 'Блок создан').click({
                 force: true
             })
         })


         *
         it('delete sanatorium', () => {
             cy.server()
             cy.route('GET', '/api/crs/hotels?PageNo=1&Size=20').as('hotels')

             cy.get('.router-link-active').click()
             cy.get(':nth-child(3) > :nth-child(5) > .q-btn__wrapper > .q-btn__content').click()
             cy.get('.blocker > .q-linear-progress > .q-linear-progress__model').should('not.be.visible')
             cy.get('.workarea > :nth-child(1) > .page > :nth-child(1)').contains('Silent Hill').click()
             cy.get('[style="display: inline-block;"] > div > .q-btn > .q-btn__wrapper > .q-btn__content > .block').click()
             cy.get('div[class="q-card"]').contains('Удалить').click()

             cy.url().should('include', '/crs/admin/hotels')
             cy.get('.text-h5').should('contain', 'Отели')
             cy.wait('@hotels').then((response) => {
                 expect(response.status).to.eq(200)
                 cy.get('.noty_body').should('contain', 'Отель успешно удален.').click({
                     force: true
                 })
             })
         })
 })