//cypress run --record --key b88f267a-8ded-41b1-9fe5-8c17ff2f69f3

describe('Login', () => {
    it('Navigate to log in page', () => {
        cy.fixture('sanatop').then(data => {
            cy.log('Navigate to log in page')
            cy.visit(data.main_url)

            cy.log('input login')
            cy.get('#Username').type(data.email)
            cy.get('#Password').type(data.pass)

            cy.log('login button click')
            cy.get('.col-sm-12 > .btn-primary').click()
        })
    })
})